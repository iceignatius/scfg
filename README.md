# Simple Configure Parser (SCFG)

This is a simple configure data parser that parse the INI like data.

## Advantage

The advantage of this parser is: light and simple.

To use this library, all your needs to do is add two source files to your project,
and there are no need to handle additional make file or build configure.

This library does not allocate memory dynamically,
this may be good for embedded environments.

## Limitation

However, this library does not support some features to keep it simple,
afterall, peoples usually use complex format (for example, JSON or XML)
for complex data combinations.

The INI features which are not supported are:

* Section.
* Escape characters.
* Double quoted string.
