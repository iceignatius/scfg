#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "scfg.h"

struct marks
{
    char item_end;
    char tag_end;
    char comment;
};

static const scfg_item_t invalid_item = {0};
static const scfg_grp_t invalid_group = {0};

bool scfg_item_is_valid(const scfg_item_t *self)
{
    /**
     * @memberof scfg_item
     * @brief Check if the item is valid?
     *
     * @param self Object instance.
     * @return TRUE if the item is valid; and FALSE if not.
     */
    return self->tag && self->value;
}

const char* scfg_item_get_tag(const scfg_item_t *self)
{
    /**
     * @memberof scfg_item
     * @brief Get tag.
     *
     * @param self Object instance.
     * @return The tag string of this item.
     */
    return self->tag;
}

const char* scfg_item_get_value(const scfg_item_t *self)
{
    /**
     * @memberof scfg_item
     * @brief Get value.
     *
     * @param self Object instance.
     * @return The tag value of this item.
     */
    return self->value;
}

long scfg_item_get_int_value(const scfg_item_t *self, long failval)
{
    /**
     * @memberof scfg_item
     * @brief
     *
     * @param self      Object instance.
     * @param failval   A specific value that will be returned when error occurred.
     * @return The item value in specific format; or @a failval if convert failed.
     */
    if( !self->value ) return failval;

    char *endpos;
    long value = strtol(self->value, &endpos, 0);

    return *endpos == 0 ? value : failval;
}

double scfg_item_get_float_value(const scfg_item_t *self, double failval)
{
    /**
     * @memberof scfg_item
     * @brief
     *
     * @param self      Object instance.
     * @param failval   A specific value that will be returned when error occurred.
     * @return The item value in specific format; or @a failval if convert failed.
     */
    if( !self->value ) return failval;

    char *endpos;
    double value = strtod(self->value, &endpos);

    return *endpos == 0 ? value : failval;
}

bool scfg_item_get_bool_value(const scfg_item_t *self, bool failval)
{
    /**
     * @memberof scfg_item
     * @brief
     *
     * @param self      Object instance.
     * @param failval   A specific value that will be returned when error occurred.
     * @return The item value in specific format; or @a failval if convert failed.
     */
    if( !self->value ) return failval;

    switch( toupper(self->value[0]) )
    {
    case 'Y':
    case 'T':
    case '1':
        return true;

    case 'N':
    case 'F':
    case '0':
        return false;

    default:
        return failval;
    }
}

static
char* find_mark(char *start, size_t len, char mark)
{
    for(char *pos = start; len--; ++pos)
    {
        if( *pos == mark )
            return pos;
    }

    return NULL;
}

static
bool is_all_spaces(char *start, size_t len)
{
    for(char *pos = start; len--; ++pos)
    {
        if( *pos != ' ' )
            return false;
    }

    return true;
}

static
size_t erase_leading_spaces(char *str)
{
    size_t count = 0;
    for(; *str == ' ' && *str != 0; ++str, ++count)
        *str = 0;

    return count;
}

static
void erase_trailing_spaces(char *str)
{
    size_t len = strlen(str);
    for(char *pos = str + len - 1; len; --len, --pos)
    {
        if( *pos == ' ' )
            *pos = 0;
        else
            break;
    }
}

static
bool parse_single_item(char *start, size_t len, const struct marks *marks)
{
    start[len-1] = 0;

    char *commpos = find_mark(start, len, marks->comment);
    if( commpos )
    {
        size_t tagval_size = commpos - start;
        size_t commsize = len - tagval_size;
        memset(commpos, 0, commsize);

        len = tagval_size;
    }

    char *tag_end = find_mark(start, len, marks->tag_end);
    if( !tag_end )
    {
        bool all_are_spaces = is_all_spaces(start, len);
        if( all_are_spaces )
            memset(start, 0, len);
        return all_are_spaces;
    }

    *tag_end = 0;
    char *tag = start;
    char *val = tag_end + 1;

    tag += erase_leading_spaces(tag);
    erase_trailing_spaces(tag);

    val += erase_leading_spaces(val);
    erase_trailing_spaces(val);

    return strlen(tag) && strlen(val);
}

scfg_grp_t scfg_grp_parse(
    char    *data,
    size_t  size,
    char    item_end,
    char    tag_end,
    char    comm_mark)
{
    /**
     * @memberof scfg_grp
     * @brief Parse the source data.
     *
     * @param data      A buffer that contains the data to be parsed.
     *                  Please note that the content of the buffer
     *                  will be modified by the parser.
     * @param size      Size of the input data.
     * @param item_end  A special character to mark the end of each items.
     * @param tag_end   A special character to mark the end of tag in one item.
     * @param comm_mark A special character to notify the parser that
     *                  all characters from this mark to the end of this item
     *                  are comments to be ignored.
     * @return
     *      A valid data group that can be used to read items later; or
     *      an invalid data group if error occurred while parsing.
     */
    if( !data || !size ) return invalid_group;

    scfg_grp_t group = { data, size };
    struct marks marks = { item_end, tag_end, comm_mark };

    size_t restsize = group.size;
    char *startpos = group.buf;
    char *endpos;
    while(( endpos = find_mark(startpos, restsize, marks.item_end) ))
    {
        size_t itemsize = endpos - startpos + 1;

        if( !parse_single_item(startpos, itemsize, &marks) )
            return invalid_group;

        startpos += itemsize;
        restsize -= itemsize;
    }

    return group;
}

bool scfg_grp_is_valid(const scfg_grp_t *self)
{
    /**
     * @memberof scfg_grp
     * @brief Check if the group is valid?
     *
     * @param self Object instance.
     * @return TRUE if the group is valid; and FALSE if not.
     */
    return self->buf;
}

const char* find_non_null(const scfg_grp_t *self, const char *start)
{
    size_t head_size = start - self->buf;
    if( head_size >= self->size ) return NULL;

    size_t tail_size = self->size - head_size;
    for(const char *pos = start; tail_size--; ++pos)
    {
        if( *pos )
            return pos;
    }

    return NULL;
}

static
const char* calc_next_search_pos(const scfg_grp_t *self, const char *currpos)
{
    const char *nextpos = currpos + strlen(currpos);
    size_t size = nextpos - self->buf;
    return size <= self->size ? nextpos : NULL;
}

scfg_item_t scfg_grp_get_next_item(const scfg_grp_t *self, const scfg_item_t *curr)
{
    /**
     * @memberof scfg_grp
     * @brief Get next item.
     *
     * @param self Object instance.
     * @param curr The current item, and can be NULL to get the first item.
     * @return
     *      A valid item next to the current item; or
     *      the first item of the group if @a curr is NULL; or
     *      an invalid item if there are no more items or error occurred.
     */
    scfg_item_t item = { .next = curr ? curr->next : self->buf };
    if( !item.next ) return invalid_item;

    item.tag = find_non_null(self, item.next);
    if( !item.tag ) return invalid_item;
    item.next = calc_next_search_pos(self, item.tag);

    item.value = find_non_null(self, item.next);
    if( !item.value ) return invalid_item;
    item.next = calc_next_search_pos(self, item.value);

    return item;
}

scfg_item_t scfg_grp_find_item(const scfg_grp_t *self, const char *tag)
{
    /**
     * @memberof scfg_grp
     * @brief Find item by tag.
     *
     * @param self  Object instance.
     * @param tag   The tag be used to find item.
     * @return
     *      A valid item which have the same tag if found; or
     *      an invalid item if not found;
     */
    for(scfg_item_t item = scfg_grp_get_next_item(self, NULL);
        scfg_item_is_valid(&item);
        item = scfg_grp_get_next_item(self, &item))
    {
        if( 0 == strcmp(scfg_item_get_tag(&item), tag) )
            return item;
    }

    return invalid_item;
}

const char* scfg_grp_find_value(const scfg_grp_t *self, const char *tag)
{
    /**
     * @memberof scfg_grp
     * @brief Find item by tag, and get its value as a string.
     *
     * @param self  Object instance.
     * @param tag   The tag be used to find item.
     * @return The corresponding value; or NULL if not found.
     */
    scfg_item_t item = scfg_grp_find_item(self, tag);
    return scfg_item_is_valid(&item) ? scfg_item_get_value(&item) : NULL;
}

long scfg_grp_find_int_value(const scfg_grp_t *self, const char *tag, long failval)
{
    /**
     * @memberof scfg_grp
     * @brief Find item by tag, and get its value as an integer.
     *
     * @param self      Object instance.
     * @param tag       The tag be used to find item.
     * @param failval   A specific value that will be returned when error occurred.
     * @return
     *      The corresponding value;
     *      or @a failval if not found or value convert failed.
     */
    scfg_item_t item = scfg_grp_find_item(self, tag);
    return scfg_item_is_valid(&item) ? scfg_item_get_int_value(&item, failval) : failval;
}

double scfg_grp_find_float_value(const scfg_grp_t *self, const char *tag, double failval)
{
    /**
     * @memberof scfg_grp
     * @brief Find item by tag, and get its value as a floating point.
     *
     * @param self      Object instance.
     * @param tag       The tag be used to find item.
     * @param failval   A specific value that will be returned when error occurred.
     * @return
     *      The corresponding value;
     *      or @a failval if not found or value convert failed.
     */
    scfg_item_t item = scfg_grp_find_item(self, tag);
    return scfg_item_is_valid(&item) ? scfg_item_get_float_value(&item, failval) : failval;
}

bool scfg_grp_find_bool_value(const scfg_grp_t *self, const char *tag, bool failval)
{
    /**
     * @memberof scfg_grp
     * @brief Find item by tag, and get its value as a boolean.
     *
     * @param self      Object instance.
     * @param tag       The tag be used to find item.
     * @param failval   A specific value that will be returned when error occurred.
     * @return
     *      The corresponding value;
     *      or @a failval if not found or value convert failed.
     */
    scfg_item_t item = scfg_grp_find_item(self, tag);
    return scfg_item_is_valid(&item) ? scfg_item_get_bool_value(&item, failval) : failval;
}
