#include <assert.h>
#include <string.h>
#include <math.h>
#include "scfg.h"

#ifdef NDEBUG
#   error This program needs ASSERT function be available!
#endif

static const char source_data[] =
    "  text  =  string value  # comments  \n"
    "  integer  =  32768  \n"
    "# comment line  \n"
    "  float  = 3.14159265  \n"
    "  # comment line  \n"
    "  boolean true  =  YES  \n"
    "  boolean false  =  NO  # comments  \n"
    ;

static
void varify_items_by_iterate(const scfg_grp_t *group)
{
    // Item 1:

    scfg_item_t item = scfg_grp_get_next_item(group, NULL);
    assert( scfg_item_is_valid(&item) );
    assert( 0 == strcmp(scfg_item_get_tag(&item), "text") );

    assert( 0 == strcmp(scfg_item_get_value(&item), "string value") );
    assert( scfg_item_get_int_value(&item, -1) == -1 );
    assert( fabs(scfg_item_get_float_value(&item, 1) - 1) < 0.0001 );
    assert( !scfg_item_get_bool_value(&item, false) == !false );

    // Item 2:

    item = scfg_grp_get_next_item(group, &item);
    assert( scfg_item_is_valid(&item) );
    assert( 0 == strcmp(scfg_item_get_tag(&item), "integer") );

    assert( 0 == strcmp(scfg_item_get_value(&item), "32768") );
    assert( scfg_item_get_int_value(&item, -1) == 32768 );
    assert( fabs(scfg_item_get_float_value(&item, 1) - 32768) < 0.0001 );
    assert( !scfg_item_get_bool_value(&item, false) == !false );

    // Item 3:

    item = scfg_grp_get_next_item(group, &item);
    assert( scfg_item_is_valid(&item) );
    assert( 0 == strcmp(scfg_item_get_tag(&item), "float") );

    assert( 0 == strcmp(scfg_item_get_value(&item), "3.14159265") );
    assert( scfg_item_get_int_value(&item, -1) == -1 );
    assert( fabs(scfg_item_get_float_value(&item, 1) - 3.14159265) < 0.0001 );
    assert( !scfg_item_get_bool_value(&item, false) == !false );

    // Item 4:

    item = scfg_grp_get_next_item(group, &item);
    assert( scfg_item_is_valid(&item) );
    assert( 0 == strcmp(scfg_item_get_tag(&item), "boolean true") );

    assert( 0 == strcmp(scfg_item_get_value(&item), "YES") );
    assert( scfg_item_get_int_value(&item, -1) == -1 );
    assert( fabs(scfg_item_get_float_value(&item, 1) - 1) < 0.0001 );
    assert( !scfg_item_get_bool_value(&item, false) == !true );

    // Item 5:

    item = scfg_grp_get_next_item(group, &item);
    assert( scfg_item_is_valid(&item) );
    assert( 0 == strcmp(scfg_item_get_tag(&item), "boolean false") );

    assert( 0 == strcmp(scfg_item_get_value(&item), "NO") );
    assert( scfg_item_get_int_value(&item, -1) == -1 );
    assert( fabs(scfg_item_get_float_value(&item, 1) - 1) < 0.0001 );
    assert( !scfg_item_get_bool_value(&item, false) == !false );

    // Item end:

    item = scfg_grp_get_next_item(group, &item);
    assert( !scfg_item_is_valid(&item) );
}

static
void varify_items_by_find(const scfg_grp_t *group)
{
    assert( 0 == strcmp(scfg_grp_find_value(group, "text"), "string value") );
    assert( scfg_grp_find_int_value(group, "text", -1) == -1 );
    assert( fabs(scfg_grp_find_float_value(group, "text", 1) - 1) < 0.0001 );
    assert( !scfg_grp_find_bool_value(group, "text", false) == !false );

    assert( 0 == strcmp(scfg_grp_find_value(group, "integer"), "32768") );
    assert( scfg_grp_find_int_value(group, "integer", -1) == 32768 );
    assert( fabs(scfg_grp_find_float_value(group, "integer", 1) - 32768) < 0.0001 );
    assert( !scfg_grp_find_bool_value(group, "integer", false) == !false );

    assert( 0 == strcmp(scfg_grp_find_value(group, "float"), "3.14159265") );
    assert( scfg_grp_find_int_value(group, "float", -1) == -1 );
    assert( fabs(scfg_grp_find_float_value(group, "float", 1) - 3.14159265) < 0.0001 );
    assert( !scfg_grp_find_bool_value(group, "float", false) == !false );

    assert( 0 == strcmp(scfg_grp_find_value(group, "boolean true"), "YES") );
    assert( scfg_grp_find_int_value(group, "boolean true", -1) == -1 );
    assert( fabs(scfg_grp_find_float_value(group, "boolean true", 1) - 1) < 0.0001 );
    assert( !scfg_grp_find_bool_value(group, "boolean true", false) == !true );

    assert( 0 == strcmp(scfg_grp_find_value(group, "boolean false"), "NO") );
    assert( scfg_grp_find_int_value(group, "boolean false", -1) == -1 );
    assert( fabs(scfg_grp_find_float_value(group, "boolean false", 1) - 1) < 0.0001 );
    assert( !scfg_grp_find_bool_value(group, "boolean false", false) == !false );

    assert( !scfg_grp_find_value(group, "not-existed") );
    assert( scfg_grp_find_int_value(group, "not-existed", -1) == -1 );
    assert( fabs(scfg_grp_find_float_value(group, "not-existed", 1) - 1) < 0.0001 );
    assert( !scfg_grp_find_bool_value(group, "not-existed", false) == !false );
}

int main(int argc, char *argv[])
{
    char buf[sizeof(source_data)];
    memcpy(buf, source_data, sizeof(source_data));

    scfg_grp_t group = scfg_grp_parse(buf, strlen(buf), '\n', '=', '#');
    assert( scfg_grp_is_valid(&group) );

    varify_items_by_iterate(&group);
    varify_items_by_find(&group);

    return 0;
}
