/**
 * @file
 * @brief       Simple configure parser.
 * @author      王文佑
 * @date        2019/05/09
 * @copyright   ZLib Licence
 * @see         https://gitlab.com/iceignatius/scfg
 */
#ifndef _SCFG_H_
#define _SCFG_H_

#include <stddef.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @class scfg_item
 * @brief Data item.
 */
typedef struct scfg_item
{
    const char *tag;
    const char *value;
    const char *next;
} scfg_item_t;

bool scfg_item_is_valid(const scfg_item_t *self);

const char* scfg_item_get_tag(const scfg_item_t *self);
const char* scfg_item_get_value(const scfg_item_t *self);
long        scfg_item_get_int_value(const scfg_item_t *self, long failval);
double      scfg_item_get_float_value(const scfg_item_t *self, double failval);
bool        scfg_item_get_bool_value(const scfg_item_t *self, bool failval);

/**
 * @class scfg_grp
 * @brief Parsed data group.
 */
typedef struct scfg_grp
{
    char    *buf;
    size_t  size;
} scfg_grp_t;

scfg_grp_t scfg_grp_parse(
    char    *data,
    size_t  size,
    char    item_end,
    char    tag_end,
    char    comm_mark);

bool scfg_grp_is_valid(const scfg_grp_t *self);

scfg_item_t scfg_grp_get_next_item(const scfg_grp_t *self, const scfg_item_t *curr);
scfg_item_t scfg_grp_find_item(const scfg_grp_t *self, const char *tag);

const char* scfg_grp_find_value(const scfg_grp_t *self, const char *tag);
long        scfg_grp_find_int_value(const scfg_grp_t *self, const char *tag, long failval);
double      scfg_grp_find_float_value(const scfg_grp_t *self, const char *tag, double failval);
bool        scfg_grp_find_bool_value(const scfg_grp_t *self, const char *tag, bool failval);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
